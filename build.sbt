scalaVersion := "2.13.10"
val AkkaVersion = "2.7.0"
val AkkaHttpVersion = "10.4.0"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion,
  "com.typesafe.slick" %% "slick" % "3.4.1",
  "com.h2database" % "h2" % "2.1.214",
  "ch.qos.logback" % "logback-classic" % "1.4.5",
  "org.postgresql" % "postgresql" % "42.5.1",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.4.1"
)

// set the main class for 'sbt run'
mainClass in (Compile, run) := Some("Entry")
// set the main class for 'sbt initData'
TaskKey[Unit]("initData") := (runMain in Compile)
  .toTask(" team.odds.message.slick.InitialDatabase")
  .value
