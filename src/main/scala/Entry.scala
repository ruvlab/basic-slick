import akka.http.scaladsl.Http
import team.odds.message.basic.AkkaComponent
import team.odds.message.routing.Routes

import scala.concurrent.Future
import scala.io.StdIn

object Entry extends App with Routes with AkkaComponent {
  private val bindingFuture: Future[Http.ServerBinding] = Http().newServerAt("localhost", 8080).bind(route)
  println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
  StdIn.readLine()
  bindingFuture
    .flatMap(_.unbind())
    .onComplete(_ => system.terminate())
}
