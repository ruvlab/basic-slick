package team.odds.message.routing
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import team.odds.message.basic.{AkkaComponent, JsonEnDecode}
import team.odds.message.models.Message
import team.odds.message.repository.MessageRepository

import scala.concurrent.Future
trait Routes extends JsonEnDecode with MessageRepository {
  val route: Route = pathPrefix("message") {
    concat(
      doGetAll,
      doCreate,
      doGet,
      doDelete
    )
  }
  private val doGetAll: Route = get {
    pathEnd {
      val mayBeAllMessages = getAllMessages
      handleMessageList(mayBeAllMessages)
    }
  }
  private val doCreate: Route = post {
    pathEnd {
      entity(as[Message]) { message =>
        val mayBeNewMessage = createNewMessage(message)
        onSuccess(mayBeNewMessage) {
          case id: Option[Int] => complete(id.getOrElse(0).toString)
          case _               => complete(StatusCodes.InternalServerError)
        }
        complete(message)
      }
    }
  }
  private val doGet: Route = get {
    path(LongNumber) { targetId =>
      val mayBeAllMessages = getAMessage(targetId)
      handleMessageList(mayBeAllMessages)
    }
  }

  private val doDelete: Route = delete {
    path(LongNumber) { targetId =>
      val maybeResult = deleteMessage(targetId)
      onSuccess(maybeResult) {
        case rowEffected: Int => complete(s"Deleted $rowEffected")
        case _                => complete(StatusCodes.NotFound)
      }
    }
  }

  private def handleMessageList(
      mayBeAllMessages: Future[Seq[Message]]
  ): Route = {
    onSuccess(mayBeAllMessages) {
      case messages: Seq[Message] => {
        complete(messages.toList)
      }
      case _ => complete(StatusCodes.NotFound)
    }
  }
}
