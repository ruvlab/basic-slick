package team.odds.message.slick

import slick.jdbc.H2Profile
import slick.jdbc.H2Profile.api._
import slick.sql.SqlProfile.ColumnOption.SqlType
import team.odds.message.models.Message

final class MessageTable(tag: Tag) extends Table[Message](tag, "message") {
  def id = column[Long]("id", O.PrimaryKey,  O.AutoInc)
  def sender = column[String]("sender")
  def content = column[String]("content")
  override def * = (sender, content, id) <> (Message.tupled, Message.unapply)
}
