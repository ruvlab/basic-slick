package team.odds.message.models

final case class Message(sender: String, content: String, id: Long = 0L)
