package team.odds.message.basic

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{RootJsonFormat, _}
import team.odds.message.models.Message
trait JsonEnDecode extends SprayJsonSupport with DefaultJsonProtocol{
  implicit val messageFormat: RootJsonFormat[Message] = jsonFormat3(Message)
}
