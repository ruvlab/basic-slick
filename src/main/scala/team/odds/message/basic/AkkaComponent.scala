package team.odds.message.basic

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors

import scala.concurrent.ExecutionContextExecutor

trait AkkaComponent {
  // needed to run the route
  implicit val system: ActorSystem[Nothing] =
    ActorSystem(Behaviors.empty, "SprayExample")
  // needed for the future map/flatmap in the end and future in fetchItem and saveOrder
  implicit val executionContext: ExecutionContextExecutor =
    system.executionContext
}
